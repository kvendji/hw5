from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Teachers',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True,
                                        serialize=False,
                                        verbose_name='ID')),
                ('first_name', models.CharField(max_length=255,
                                                verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255,
                                               verbose_name='Фамилия')),
                ('age', models.IntegerField(verbose_name='Возраст')),
            ],
            options={
                'verbose_name': 'Учитель',
                'verbose_name_plural': 'Учителя',
            },
        ),
    ]
