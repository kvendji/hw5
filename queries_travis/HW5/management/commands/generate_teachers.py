from HW5.models import Teachers

from django.core.management.base import BaseCommand

from faker import Faker


class Command(BaseCommand):

    def handle(self, *args, **options):
        fake = Faker('ru-RU')
        teachers = []
        for _ in range(100):
            boolean = fake.boolean(chance_of_getting_true=50)
            age = fake.random_int(min=22, max=60, )
            if boolean:
                first_name = fake.first_name_female()
                last_name = fake.last_name_female()
            else:
                first_name = fake.first_name_male()
                last_name = fake.last_name_male()
            some_teacher = Teachers(first_name=first_name,
                                    last_name=last_name,
                                    age=age)
            teachers.append(some_teacher)
        Teachers.objects.bulk_create(teachers)
