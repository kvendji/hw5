from HW5.models import Teachers

from django.db.models import Q
from django.views.generic.base import TemplateView


class TeachersView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        teachers = Teachers.objects.all()
        context = {
            'teachers': teachers
        }
        if 'search_by' in self.request.GET:
            teachers = teachers.filter(
                Q(first_name__icontains=self.request.GET['search_by']) |
                Q(last_name__icontains=self.request.GET['search_by'])
            )
            context['teachers'] = teachers
        if self.request.GET.get('max_age'):
            teachers = teachers.filter(
                age__gte=self.request.GET['min_age'],
                age__lt=self.request.GET['max_age']
            )
            context['teachers'] = teachers

        return context
